function startKategorie() {
    let url;
    if (window.location.toString().includes('localhost')) {
        url = 'http://localhost:80/products/';
    } else {
        url = 'https://hausarbeitgameshopheroku.herokuapp.com/products/';
    }
    let urlParams = new URLSearchParams(window.location.search);
    const kategoriename = urlParams.get('kategorie');
    fetch(url, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            window.location.href = '../webseiten/fehler.html';
            return;
        }
        response.json().then(function (artikel) {

            //Navbar anpassen
            var navlinks = document.getElementsByClassName('nav-link');
            for (i of navlinks) {
                if (i.innerHTML === kategoriename) {
                    i.parentElement.classList.add('active');
                } else {
                    i.parentElement.classList.remove('active')
                }
            }
            //Generiere Artikel
            let kategoriegefunden = true;
            for (let i of artikel) {
                if (i.category === kategoriename) {
                    createKategorieEintrag(i);
                    kategoriegefunden = false;
                }
            }
            if (kategoriegefunden) {
                window.location.href = '../webseiten/fehler.html';
            }
            //Titel Einfügen
            document.getElementById('kategorieTitel').innerHTML = kategoriename;
        })
    });
}


function createKategorieEintrag(product) {
    let newRow = document.createElement('div');
    newRow.className = 'row mb-5 kategorieArtikel';


    //Bild Elemente
    let newPictureCol = document.createElement('div');
    newPictureCol.className = 'col-12 col-md-6 text-center';
    let newPicturelink = document.createElement('a');
    newPicturelink.href = 'artikel.html?id=' + product.id;
    let newPicture = document.createElement('img');
    newPicture.className = 'image' + product.category;
    newPicture.alt = 'Produktbild';
    newPicture.src = product.image;

    newPicturelink.appendChild(newPicture);
    newPictureCol.appendChild(newPicturelink);
    newRow.appendChild(newPictureCol);

    //Text Elemente
    let newTextCol = document.createElement('div');
    newTextCol.className = 'col-12 col-md-6';
    let newLink = document.createElement('a');
    newLink.href = 'artikel.html?id=' + product.id;
    let newh2 = document.createElement('h2');
    newh2.innerHTML = product.name;
    let newKurzbeschreibung = document.createElement('p');
    newKurzbeschreibung.innerHTML = product.kurzbeschreibung;
    let newh4 = document.createElement('h4');
    newh4.innerHTML = 'Preis ' + product.price.value + ' ' + product.price.currency;
    let newButtonDiv = document.createElement('div');
    newButtonDiv.className = 'kategorieBottons';
    let newKaufButton = document.createElement('button');
    newKaufButton.innerHTML = 'In den Einkaufswagen';
    newKaufButton.type = 'button';
    newKaufButton.id = 'buttonbuy';
    newKaufButton.className = 'btn btn-outline-secondary popup';
    newKaufButton.addEventListener('click', function () {
        postToCart(product.id, 1);
       // this.classList.toggle('show');
      this.innerHTML = "&#10004";
        setTimeout( function () {
            newKaufButton.innerHTML = 'In den Einkaufswagen';
        }, 1000);

    });
    let newProduktdetails = document.createElement('button');
    newProduktdetails.innerHTML = 'Produkt Details';
    newProduktdetails.type = 'button';
    newProduktdetails.className = 'btn btn-outline-secondary';
    newProduktdetails.addEventListener('click', function () {
        window.location.href = 'artikel.html?id=' + product.id;
    });
    newButtonDiv.appendChild(newKaufButton);
    newButtonDiv.appendChild(newProduktdetails);
    newLink.appendChild(newh2);
    newLink.appendChild(newKurzbeschreibung);
    newLink.appendChild(newh4);
    newTextCol.appendChild(newLink);
    newTextCol.appendChild(newButtonDiv);
    newRow.appendChild(newTextCol);

    document.getElementById('produktContainer').appendChild(newRow);
}

function postToCart(productId, anzahl) {
    let url;
    if (window.location.toString().includes('localhost')) {
        url = 'http://localhost:80/cart/';
    } else {
        url = 'https://hausarbeitgameshopheroku.herokuapp.com/cart/';
    }

    var data = {id: productId, anz: anzahl};

    fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {'Content-Type': 'application/json'}
    }).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
        }
    });


}