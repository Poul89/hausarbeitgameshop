function StartCart() {

    let url;
    let carturl;
    let delurl;
    let buyurl;
    if (window.location.toString().includes('localhost')) {
        url = 'http://localhost:80/products/';
        carturl = 'http://localhost:80/cart/';
        delurl = 'http://localhost:80/delete/';
        buyurl = 'http://localhost:80/buy/';
    } else {
        url = 'https://hausarbeitgameshopheroku.herokuapp.com/products/';
        carturl = 'https://hausarbeitgameshopheroku.herokuapp.com/cart/';
        delurl = 'https://hausarbeitgameshopheroku.herokuapp.com/delete/';
        buyurl = 'https://hausarbeitgameshopheroku.herokuapp.com/buy/';
    }
    let summe = 0;

    let btnbuy = document.getElementById("buy");
    btnbuy.addEventListener('click',function () {
        buy(buyurl);
    });

    fetch(carturl, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }
        response.json().then(function (actcart) {
            let i = 0;
            actcart.forEach(function (cartitem) {
                const productid = cartitem.id;
                let actindex = cartitem.index;
                const prourl = url + productid;

                fetch(prourl, {method: 'GET'}).then(response => {
                    if (response.status !== 200) {
                        console.log('Looks like there was a problem. Status Code: ' + response.status);
                        window.location.href = '../webseiten/fehler.html';
                        return;
                    }
                    response.json().then(function (artikel) {
                        additem(artikel.name, '../' + artikel.image, artikel.price.value, cartitem.anz, actindex, delurl);
                        i++;
                        summe = summe + (artikel.price.value * cartitem.anz);
                        if (i === actcart.length) {
                            document.getElementById('BetragTotal').innerHTML = summe.toString();
                        }
                    })
                });
            });
        })
    });
    window.setTimeout(checkitems,500);
    //checkitems();
}

function deleteInCart(index, delurl) {

    fetch(delurl + index, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            window.location.href = '../webseiten/fehler.html';
            return;
        }
        response.json().then(function (a) {

        })
    });
    location.reload();
}

function buy(buyurl){

    fetch( buyurl, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }
        response.json().then(function (a) {

        })
    });
    alert('Danke für das Interesse! \n Leider sind im Moment keine Artikel auf Lager');
    location.reload();

}

function checkitems(){
    let items = document.getElementsByTagName('img').length;
    if(items >= 1){
        document.getElementById('buy').disabled = false;
    } else{ document.getElementById('buy').disabled = true; }
}

function additem(name, src, preis, anzahl, index, delurl) {

    //https://jsfiddle.net/bootstrapious/wc6g251x
    var body = document.getElementById("body");
    var tablerow = document.createElement("tr");
    var tablehead = document.createElement("th");
    tablehead.scope = "row";
    tablehead.classList.add("border-0");

    var div1 = document.createElement("div");
    div1.classList.add("p-2");

    var img = document.createElement("img");
    img.src = src;//"../images/bannerMaus.png"; //src
    img.width = 100;
    img.height = 150;
    img.classList.add("img-fluid");
    img.style.marginBottom = '1em';

    var div2 = document.createElement("div");
    div2.classList.add('ml-3', 'd-inline-block', 'align-middle');
    var newh5 = document.createElement("h5");
    newh5.classList.add("mb-03");
    newh5.innerHTML = name;//"Beschreibung"; //name
    div2.appendChild(newh5);
    div1.appendChild(img);
    div1.appendChild(div2);
    tablehead.appendChild(div1);

    var td1 = document.createElement("td");
    td1.classList.add("border-0", "align-middle", "einzelpreis");
    td1.id = 'stkpreis';
    td1.innerHTML = preis;//"35€"; //Preis
    var td2 = document.createElement("td");
    td2.classList.add("border-0", "align-middle");
    td2.innerHTML = anzahl;//"1"; //Anzahl
    td2.id = 'anza';
    var td3 = document.createElement("td");
    td3.classList.add('border-0', 'align-middle');
    var a = document.createElement("a");
    a.href = '#';
    a.classList.add("text.dark");
    a.addEventListener('click', function () {
        deleteInCart(index, delurl)
    });
    var i = document.createElement("i");
    i.classList.add("fa", "fa-trash");
    a.appendChild(i);
    td3.appendChild(a);

    tablerow.appendChild(tablehead);
    tablerow.appendChild(td1);
    tablerow.appendChild(td2);
    tablerow.appendChild(td3);
    body.appendChild(tablerow);
}
