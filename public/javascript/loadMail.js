
let url;

function sendmail() {
        var mailuser = document.getElementById("InputEmail").value;
        var username = document.getElementById("InputUsername").value;
        var useranfrage = document.getElementById("InputAnfrage").value;

        if (window.location.toString().includes('localhost')) {
            url = 'http://localhost:80/mail/';
        } else {
            url = 'https://hausarbeitgameshopheroku.herokuapp.com/mail/';
        }

        var data = {muser: mailuser, uname: username, ufrage: useranfrage};

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        }).then(response => {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' + response.status);
                window.location.href = '../webseiten/fehler.html';
            }
        });
}


function checksend(){
    if(usernamevalid&&emailvalid){
        document.getElementById("submitmailanfrage").disabled = false;
    }
}