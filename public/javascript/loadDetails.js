const url = 'https://hausarbeitgameshopheroku.herokuapp.com/products/';
//const url = 'http://localhost:80/products/';
const rezurl = '../allgemeines/rezensionErfassen.html';
var urlParams = new URLSearchParams(window.location.search);
const productid = urlParams.get('id');
const searchurl = url + productid;
const urlextension = window.location.search;

function startArtikel() {
    let url;
    if (window.location.toString().includes('localhost')) {
        url = 'http://localhost:80/products/';
    } else {
        url = 'https://hausarbeitgameshopheroku.herokuapp.com/products/';
    }

    const urlParams = new URLSearchParams(window.location.search);
    const productid = urlParams.get('id');
    const searchurl = url + productid;

    fetch(searchurl, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            window.location.href = '../webseiten/fehler.html';
            return;
        }
        response.json().then(function (artikel) {
            generateSite(artikel)
        })
    });
}

function generateSite(artikel) {
    //Titel anpassen
    document.getElementById('titelKategorie').innerHTML = artikel.name;

    //Bild anpassen
    document.getElementById('productId').setAttribute('class', 'image' + artikel.category);
    document.getElementById('productId').setAttribute('src', artikel.image);

    //Navbar anpassen
    var navlinks = document.getElementsByClassName('nav-link');
    for (i of navlinks) {
        if (i.innerHTML === artikel.category) {
            i.parentElement.classList.add('active');
        } else {
            i.parentElement.classList.remove('active')
        }
    }

    //Preis Anpassen
    let pricetext = 'Price ' + artikel.price.value + '.- ' + artikel.price.currency;
    document.getElementById('price').innerHTML = pricetext;

    //Kurzbeschreibung anpassen
    document.getElementById('productSpecs').innerHTML = artikel.kurzbeschreibung;

    //Rezensions URL anpassen
    document.getElementById('rezensionButton').addEventListener('click', function () {
        window.location.href = rezurl + urlextension;
    });

    //Kaufbutton anpassen
    document.getElementById('kaufButton').addEventListener('click', function () {
        this.innerHTML = "&#10004";
        setTimeout( function () {
            document.getElementById('kaufButton').innerHTML = 'Kaufen';
        }, 1000);
        posttocart(artikel.id, document.getElementById('anzahlwahl').value);
    });

    //Beschreibungstext anpassen
    document.getElementById('beschreibungText').innerHTML = artikel.beschreibung;

    //Spezifikationen anpassen
    var spezifikationen = artikel.spezifikation;
    for (var prop in spezifikationen) {
        createSpecRow(prop, spezifikationen[prop]);
    }

    //Rezensionanpassen
    var rezensionen = artikel.rezension;
    for (var i = 0; i < artikel.rezension.benutzername.length; i++) {
        let newrezension = generateRezensionen(rezensionen, i);
        newrezension.style.marginTop = '1em';
        if (i === artikel.rezension.benutzername.length - 1) {
            newrezension.style.borderBottom = 'none';
        }
        document.getElementById('Rezensionstext').appendChild(newrezension);
    }

    //Tab Rezension öffnen
    const urlParams = new URLSearchParams(window.location.search);
    const rezinlink = urlParams.get('rez');
    if(rezinlink !== null){
        document.getElementById('headingThree').
        firstElementChild.firstElementChild.click();
    }
}

function posttocart(productId, anzahl) {
    let url;
    if (window.location.toString().includes('localhost')) {
        url = 'http://localhost:80/cart/';
    } else {
        url = 'https://hausarbeitgameshopheroku.herokuapp.com/cart/';
    }

    var data = {id: productId, anz: anzahl};

    fetch(url, {method: 'POST', body: JSON.stringify(data), headers: {'Content-Type':'application/json'}}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            window.location.href = '../webseiten/fehler.html';
        }
    });}

function createSpecRow(bez, wert) {
    let newTr = document.createElement('tr');
    let newTdLeft = document.createElement('td');
    let newTdRight = document.createElement('td');
    newTdLeft.className = 'left';
    newTdLeft.innerHTML = bez;
    newTdRight.innerHTML = wert;
    newTr.appendChild(newTdLeft);
    newTr.appendChild(newTdRight);
    document.getElementById('spectable').appendChild(newTr);
}

function generateRezensionen(rezension, index) {
    var newdiv = document.createElement('div');
    newdiv.className = 'rezensionKlasse';
    var newh4 = document.createElement('h4');
    newh4.innerHTML = rezension.benutzername[index];
    newdiv.appendChild(newh4);
    var stardiv = createStars(rezension.anzahlSterne[index], index);
    // stardiv.className = 'form-group';
    newdiv.appendChild(stardiv);

    var newreztext = document.createElement('p');
    newreztext.innerHTML = rezension.bewertungsText[index];
    newdiv.appendChild(newreztext);

    return newdiv;
}

//Erstellt ein Div mit den Anzahl sternen
function createStars(sterne, index) {
    let hauptspan = document.createElement('span');
    hauptspan.className = 'sternebewertung';
    hauptspan.id = 'anzahlSterne' + index;
    for (var i = 4; i >= 0; i--) {
        let newinput = document.createElement('input');
        newinput.type = 'checkbox';
        newinput.id = 'stern' + index + '_' + (i + 1);
        newinput.name = 'bewertung';
        newinput.value = i + 1;
        newinput.disabled = true;
        if (i + 1 === sterne) {
            newinput.checked = true;
        }
        hauptspan.appendChild(newinput);

        let newlabel = document.createElement('label');
        newlabel.htmlFor = 'stern' + index + '_' + (i + 1);
        newlabel.title = (i + 1) + ' Stern';
        newlabel.innerHTML = (i + 1) + ' Stern';
        hauptspan.appendChild(newlabel);
    }
    var hauptdiv = document.createElement('div');
    hauptdiv.appendChild(hauptspan);
    hauptdiv.style.height = '3em';
    return hauptdiv;
}