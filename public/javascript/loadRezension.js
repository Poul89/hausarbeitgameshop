var usernamevalid = false;
var emailvalid = false;
var rezensionvalid = false;
var sternevalid = false;

let num;
let username;
let email;
let rezensiontext;

function check() {
    var submit = document.getElementById('submit');
    //abprüfen
    if (usernamevalid && emailvalid && rezensionvalid) {
        submit.disabled = false;
        submit.addEventListener("click", function (event) {
            event.preventDefault();
            sternevalidation();
            if (num == 0) {
                let conf = confirm("Wollen Sie diesen Artikel wirklich mit 0 Sternen bewerten?");
                if (conf == true) {
                    //alert("true");
                    submitsend();
                } else { //alert("false");
                    return false;
                }
                //alert("after");
            } else {
                submitsend();
            }
        })
    }
}

//Username: ausgefüllt, mind 4 Zeichen
function usernamevalidation() {
    var myUsername = document.getElementById('InputUsername').value;
    if (myUsername == "" || myUsername.length <= 3) {
        document.getElementById('InputUsername').style.backgroundColor = "#ffdfe1";
        document.getElementById("errorUsername").innerHTML = "Benutzername muss zwingend ausgefüllt werden und mindestens 4 Zeichen lang sein";
        usernamevalid = false;
        //return false;
    } else {
        document.getElementById('InputUsername').style.backgroundColor = "#ffffff";
        document.getElementById("errorUsername").innerHTML = "";
        usernamevalid = true;
        username = myUsername;
        //return true;
    }
}

//Email: ausgefüllt, https://www.w3resource.com/javascript/form/email-validation.php
function emailvalidation() {
    var myEmail = document.getElementById('InputEmail').value;
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myEmail)) {
        document.getElementById('InputEmail').style.backgroundColor = "#ffffff";
        document.getElementById("errorEmail").innerHTML = "";
        emailvalid = true;
        email = myEmail;
        document.getElementById("submitmail").disabled
        //return true;
    } else {
        document.getElementById('InputEmail').style.backgroundColor = "#ffdfe1";
        document.getElementById("errorEmail").innerHTML = "Gib eine gültgie Email Adresse ein!";
        emailvalid = false;
        //return false;
    }
}

//Rezension: ausgefüllt, max 50 Zeichen
function rezensionvalidation() {
    var myRezension = document.getElementById('InputRezension').value;
    if (myRezension.length > 50 || myRezension.length < 10) {
        document.getElementById('InputRezension').style.backgroundColor = "#ffdfe1";
        document.getElementById("errorRezension").innerHTML = "Die Rezension muss mindestens 10 und darf maximal 50 Zeichen lang sein!";
        rezensionvalid = false;
        //return false;
    } else {
        document.getElementById('InputRezension').style.backgroundColor = "#ffffff";
        document.getElementById("errorRezension").innerHTML = "";
        rezensionvalid = true;
        rezensiontext = myRezension;
        //return true;
    }
}


//Sterneberwertung: ausgefüllt
function sternevalidation() {
    let stars = document.getElementsByName('bewertung');
// TEST
    num = 0;
    for (let i = 0; i < stars.length; i++) {
        if (stars[i].checked) {
            num = stars[i].value;
            //alert(num);
        }
    }
}

function submitsend() {
    //alert("submitsenden");
    let urlParams = new URLSearchParams(window.location.search);
    urlParamsId = urlParams.get("id");

    let url;
    if (window.location.toString().includes('localhost')) {
        url = 'http://localhost:80/products/' + urlParamsId + '/reviews';
    } else {
        url = 'https://hausarbeitgameshopheroku.herokuapp.com/products/' + urlParamsId + '/reviews';
    }

   // alert(url + username + rezensiontext + num);
    const rezension = {id: urlParamsId, persname: username, text: rezensiontext, anzSterne: num};
    fetch(url, {
        method: "POST", body: JSON.stringify(rezension), headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            window.location.href = '../webseiten/fehler.html';
        } else {
            console.log('Rezension wurde abgesendet');
            window.location.href = '../webseiten/artikel.html?id=' + urlParamsId + '&rez=1#headingThree';
            //alert("Antwort vom post ist ok" +response.status);
        }

    })

}

function loadProductName() {
    let url;
    if (window.location.toString().includes('localhost')) {
        url = 'http://localhost:80/products/';
    } else {
        url = 'https://hausarbeitgameshopheroku.herokuapp.com/products/';
    }
    const urlParams = new URLSearchParams(window.location.search);
    const productid = urlParams.get('id');
    const searchurl = url + productid;

    fetch(searchurl, {method: 'GET'}).then(response => {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            window.location.href = '../webseiten/fehler.html';
            return;
        }
        response.json().then(function (artikel) {
            let ueberschrift = document.createElement('h2');
            ueberschrift.innerHTML = 'Rezension für: ' + artikel.name;
            document.getElementById('productname').appendChild(ueberschrift);
        })
    });
}