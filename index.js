const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(express.static('./public'));
let products = require('./public/json/products.json');
var cart = [];
var cartindex =0;
const nodemailer = require('nodemailer');

app.use(bodyParser.json());
app.get('/', (req, res) => res.send('Hello World!'));

//Alle Produkte zurückgeben
app.get('/products', (req, res) => res.send(products));

//Produkt mit bestimmter ID zurückgeben
app.get('/products/:id', function (req, res) {
    let idgesucht = req.params.id;
    let responsesend = false;
    for (var i of products) {
        if (i.id == idgesucht) {
            res.send(JSON.stringify(i));
            responsesend = true;
        }
    }
    if (!responsesend) {
        res.sendStatus(404);
    }
});

//Review hinzufügen
app.post('/products/:id/reviews', function (req, res) {
    let idgesucht = req.params.id;
    console.log(idgesucht);
    let responsesend = false;
    for (let i of products) {
        if (i.id == idgesucht) {
            let review = req.body;
            i.rezension.benutzername.push(review.persname);
            i.rezension.anzahlSterne.push(parseInt(review.anzSterne));
            i.rezension.bewertungsText.push(review.text);
            res.send(console.log('Rezension wurde erfasst'));
            responsesend = true;
        }
    }
    if (!responsesend) {
        res.sendStatus(404);
    }
});

//Warenkorb zurückgeben
app.get('/cart', (req, res) => res.send(cart));

//Element zu Warenkorb hinzufügen
app.post('/cart', function (req, res) {
    let daten = req.body;
    let prodid = daten.id;
    let prodanz = daten.anz;
    let actindex = cartindex++;
    let newcartpr = new Cartproduct(prodid, prodanz,actindex);
    cart.push(newcartpr);
    res.send(console.log('artikel wurde hinzugefügt'));
});

//Element aus Warenkorb löschen
app.get('/delete/:index', function (req, res) {
    let position = req.params.index;
    let responsesend = false;
    for(var i = 0; i < cart.length; i++) {
        if(cart[i].index == position) {
            cart.splice(i, 1);
            responsesend = true;
            break;
        }
    }
    if (!responsesend) {
        res.sendStatus(404);
    }
    res.send('geloescht');
});

//Einkauf abschliessen
app.get('/buy', function (req, res){
    cart = [];
    res.send('gekauft');
});

//Mail versenden
app.post('/mail',function (req,res) {
    let daten = req.body;
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: "gameshophausarbeit@gmail.com",
            pass: 'hausarbeitgameshop'
        }
    });

    var mailOptions = {
        from: 'gameshophausarbeit@gmail.com',
        to: daten.muser,
        subject: 'Bestätigung GameShop',
        text: 'Hallo ' + daten.uname + '   danke für die Anfrage über den GameShop:  ' + daten.ufrage
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            res.sendStatus(404);
        } else {
            console.log('Email sent: ' + info.response);
            alert('Email sent: ' + info.response);
        }
    });
    res.send('gesendet');

});

const PORT = process.env.PORT || 80;
app.listen(PORT, () => console.log('Example app listening on port' + PORT));

function Cartproduct(id, anz,ind) {
    this.id = id;
    this.anz = anz;
    this.index = ind
}

//Fehlerhandling bei komplett falschem link
app.use(function(req, res, next) {
    res.status(404).sendFile('webseiten/fehler.html', { root: './public/' });
});